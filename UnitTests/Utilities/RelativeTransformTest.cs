﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;

namespace UnitTests.Utilities
{
    [TestClass]
    public class RelativeTransformTest
    {
        [TestMethod]
        public void RelativeTransformParentTest()
        {
            RelativeTransform relativeTransform = new RelativeTransform
            {
                //Test translation
                Position = new Vector3(10, 5, 7)
            };
            Matrix matrix = relativeTransform.Transform;

            Assert.AreEqual(new Vector3(10, 5, 7), matrix.Translation);

            //Test rotation
            relativeTransform.Rotation = (float)Math.PI;
            matrix = relativeTransform.Transform;

            Assert.AreEqual(new Vector3((float)Math.Cos((float)Math.PI), (float)Math.Sin((float)Math.PI),  0), matrix.Right);

            //Test scailing
            relativeTransform.Scale = new Vector3(3, 3, 3);
            matrix = relativeTransform.Transform;

            Assert.AreEqual(new Vector3(0, 0, 3), matrix.Backward);

            //Verifiy complete matrix
            matrix = Matrix.CreateRotationZ((float)Math.PI) *
                Matrix.CreateScale(new Vector3(3, 3, 3)) *
                Matrix.CreateTranslation(new Vector3(10, 5, 7));

            Assert.AreEqual(matrix, relativeTransform.Transform);

            //Test Parenting
            RelativeTransform parentTransform = new RelativeTransform
            {
                Position = new Vector3(6, 24, 7),
                Rotation = (float)Math.PI
            };

            relativeTransform.Parent = parentTransform;

            matrix = Matrix.CreateRotationZ((float)Math.PI) *
                Matrix.CreateScale(new Vector3(3, 3, 3)) *
                Matrix.CreateTranslation(new Vector3(10, 5, 7)) *
                parentTransform.Transform;

            Assert.AreEqual(matrix, relativeTransform.Transform);

            matrix = Matrix.CreateRotationZ((float)Math.PI) *
                Matrix.CreateScale(new Vector3(3, 3, 3)) *
                Matrix.CreateTranslation(new Vector3(10, 5, 7));

            Assert.AreEqual(matrix, relativeTransform.LocalTransform);
        }
        [TestMethod]
        public void GameObjectParentingTest()
        {
            GameObject parent = new GameObject();
            GameObject child = new GameObject();
            Assert.AreNotSame(null, child.Transforms);
            parent.AddChild(child);
            parent.Transforms.Position = new Vector3(90, 45, 0);

            Assert.AreEqual(new Vector3(90, 45, 0), child.Transforms.Transform.Translation);

            parent.Transforms.Scale = new Vector3(4, 4, 4);
            parent.Transforms.Rotation = (float)Math.PI;
            child.Transforms.Rotation = (float)Math.PI / 2;
            Matrix matrix = Matrix.CreateRotationZ((float)Math.PI / 2) *
                Matrix.CreateScale(new Vector3(1, 1, 1)) *
                Matrix.CreateTranslation(new Vector3(0, 0, 0)) *
                parent.Transforms.Transform;

            Assert.AreEqual(matrix, child.Transforms.Transform);
        }
    }
}
