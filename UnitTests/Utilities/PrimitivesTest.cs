﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Utilities;

namespace UnitTests.Utilities
{
    [TestClass]
    public class PrimitivesTest
    {
        [TestMethod]
        public void RectanglePrimitiveTest()
        {
            PrimitiveGeometry geometry = RectanglePrimitive.GetGeometry(50, 100, Color.Red);

            Assert.IsNotNull(geometry.vertices);
            Assert.IsNotNull(geometry.indices);
            Assert.AreEqual(4, geometry.vertices.Length);
            Assert.AreEqual(6, geometry.indices.Length);
            Assert.AreEqual(new VertexPositionColor(new Vector3(0, 0, 0), Color.Red), geometry.vertices[0]);
            Assert.AreEqual(new VertexPositionColor(new Vector3(50, 0, 0), Color.Red), geometry.vertices[1]);
            Assert.AreEqual(new VertexPositionColor(new Vector3(50, 100, 0), Color.Red), geometry.vertices[2]);
            Assert.AreEqual(new VertexPositionColor(new Vector3(0, 100, 0), Color.Red), geometry.vertices[3]);
            Assert.AreEqual(0, geometry.indices[0]);
            Assert.AreEqual(1, geometry.indices[1]);
            Assert.AreEqual(3, geometry.indices[2]);
            Assert.AreEqual(1, geometry.indices[3]);
            Assert.AreEqual(2, geometry.indices[4]);
            Assert.AreEqual(3, geometry.indices[5]);
            Assert.AreEqual(2, geometry.TriangleCount);
        }
    }
}
