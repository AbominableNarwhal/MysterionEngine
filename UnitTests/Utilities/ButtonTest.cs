﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine.Utilities;

namespace UnitTests.Utilities
{
    [TestClass]
    public class ButtonTest
    {
        [TestMethod]
        public void ButtonInputTest()
        {
            RelativeTransform transform = new RelativeTransform();
            Button button = new Button(null, null, null, transform)
            {
                Bounds = new RotatedRectangle(transform, 0 , 0, 50, 50)
            };

            Assert.IsFalse(button.Pressed);

            int clickEventCount = 0;
            int cancelEventCount = 0;
            button.Subscribe(delegate (object sender, Button.ButtonClickEventArgs e)
            {
                if (e.EventType == Button.Events.Click)
                {
                    clickEventCount++;
                }
                else if (e.EventType == Button.Events.Cancel)
                {
                    cancelEventCount++;
                }
            });

            //Test normal clicks. The mouse should hover over the button first before a click can
            // be registered
            MouseState mouseState = new MouseState(25, 25, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(25, 25, 0, ButtonState.Pressed, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(25, 25, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            Assert.AreEqual(1, clickEventCount);

            //Test state after first click
            mouseState = new MouseState(25, 25, 0, ButtonState.Pressed, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(25, 25, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            Assert.AreEqual(2, clickEventCount);

            //Test clicks outside of bounds
            mouseState = new MouseState(100, 100, 0, ButtonState.Pressed, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);

            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(100, 100, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);

            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            Assert.AreEqual(2, clickEventCount);

            //Test cancel click
            mouseState = new MouseState(25, 25, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(25, 25, 0, ButtonState.Pressed, ButtonState.Released,
               ButtonState.Released, ButtonState.Released, ButtonState.Released);

            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(100, 100, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);

            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            Assert.AreEqual(1, cancelEventCount);

            //Test releasing inside button, moving mouse out then back in again
            mouseState = new MouseState(25, 25, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(100, 100, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(25, 25, 0, ButtonState.Pressed, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            mouseState = new MouseState(25, 25, 0, ButtonState.Released, ButtonState.Released,
                ButtonState.Released, ButtonState.Released, ButtonState.Released);
            button.OnInputUpdate(new KeyboardState(new Keys[0], false, false), mouseState);

            Assert.AreEqual(2, clickEventCount);
        }
    }
}
