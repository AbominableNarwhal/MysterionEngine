﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MonoMysterionEngine
{
    public class Activity
    {
        public virtual void Start() { }
        public virtual void Restart() { }
        public virtual void Update(GameTime gameTime) { }
        public virtual void Pause() { }
        public virtual void Destroy() { }
        public virtual void RenderBackground() { }
        public virtual void RenderForeGround() { }

        public virtual void InputUpdate(KeyboardState keyboardState, MouseState mouseState) { }
    }
}
