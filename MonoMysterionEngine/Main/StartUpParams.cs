﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine
{
    public class StartUpParams
    {
        public StartUpParams()
        {
            WindowWidth = 800;
            WindowHeight = 720;
            FullScreen = false;
            Borderless = false;
            WindowPosition = new Point(0, 0);
            FixedFps = false;
            Fps = 60f; // 20 milliseconds, or 50 FPS. 
            AppName = "";
        }
        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }
        public bool FullScreen { get; set; }
        public bool Borderless { get; set; }
        public Point WindowPosition {get;set;}
        public string AppName { get; set; }

        public bool FixedFps { get; set; }

        public float Fps { get; set; }

        public Activity StartActivity { get; set; }
    }
}
