﻿using MonoMysterionEngine.Systems;
using System;

namespace MonoMysterionEngine
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public class MysterionEngineGame
    {
        /// <summary>
        /// The main entry point for the game called by the client
        /// </summary>
        public static void Initialize(StartUpParams param)
        {
            WindowSystem.Window.Param = param;
            using (Game = new Engine())
                Game.Run();
        }
        public static Engine Game { get; set; }
    }
#endif
}
