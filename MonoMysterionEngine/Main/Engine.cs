﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Services;
using System.Collections.Generic;
using MonoMysterionEngine.Objects;
using System;

namespace MonoMysterionEngine
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Engine : Game
    {
        //Monogame variables
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //Activity stack implementation
        Stack<Activity> activityStack;
        Activity nextActivity;
        bool finishedActivity;

        public RenderService RenderService { get; private set; }
        public AnimationService AnimationService { get; private set; }
        public InputService InputService { get; private set; }
        public CollisionService CollisionService { get; private set; }
        public UpdateService UpdateService { get; private set; }
        public ServiceManager ServiceLocator { get; }
        public ObjectManager ObjectLocator { get; }
        public Engine()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            activityStack = new Stack<Activity>();
            ServiceLocator = new ServiceManager();
            ObjectLocator = new ObjectManager();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            //Inicialize Systems
            WindowSystem.Window.InitWindow(graphics, Window);
            RenderSystem.Renderer.InitRenderer(spriteBatch, Content, GraphicsDevice);
            nextActivity = WindowSystem.Window.Param.StartActivity;

            //Create and start services
            RenderService = new RenderService();
            ServiceLocator.AddService(RenderService);
            AnimationService = new AnimationService();
            ServiceLocator.AddService(AnimationService);
            InputService = new InputService();
            ServiceLocator.AddService(InputService);
            CollisionService = new CollisionService();
            ServiceLocator.AddService(CollisionService);
            UpdateService = new UpdateService();
            ServiceLocator.AddService(UpdateService);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                FinishActivity();
            }
            // Update pending activity actions
            if (finishedActivity)
            {
                if (activityStack.Count > 0)
                {
                    activityStack.Peek().Destroy();
                    activityStack.Pop();
                    finishedActivity = false;
                    if (activityStack.Count > 0)
                        activityStack.Peek().Restart();
                }
            }
            if (nextActivity != null)
            {
                if (activityStack.Count > 0)
                    activityStack.Peek().Pause();
                activityStack.Push(nextActivity);
                nextActivity.Start();
                nextActivity = null;
            }
            if (activityStack.Count == 0)
            {
                Exit();
            }
            //Input always comes first
            InputService.performInputService();
            if (activityStack.Count > 0)
                activityStack.Peek().InputUpdate(InputSystem.Input.KeyboardState, InputSystem.Input.MouseState);
            //start the frame with automated management
            AnimationService.PerformAnimationService(gameTime);
            //After objects have settled from automated managment run collision testing
            CollisionService.performCollisionService();
            //Let client update
            if(activityStack.Count > 0)
                activityStack.Peek().Update(gameTime);
            //Let Components update
            UpdateService.PerformUpdateService(gameTime);
            //Allow the object manager to apply its internal pending changes
            ObjectLocator.UpdateContents();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            RenderSystem.Renderer.ClearScreen();
            //put rendering code here
            activityStack.Peek().RenderBackground();
            RenderService.PerformRenderService();
            activityStack.Peek().RenderForeGround();
            // TODO: Add your drawing code here
           
            //float frameRate = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds;
            //Console.WriteLine("FPS: " + frameRate);

            base.Draw(gameTime);
        }
        public void StartActvity(Activity activity)
        {
            nextActivity = activity;
        }
        public void FinishActivity()
        {
            finishedActivity = true;
        }
        public static float ToRadians(float angle)
        {
            return angle * (float)(Math.PI / 180);
        }
        public static Vector2 RotateVector(Vector2 point, float rotation)
        {
            float sin = (float)Math.Sin(rotation);
            float cos = (float)Math.Cos(rotation);
            return new Vector2(point.X * cos - point.Y * sin,
                    point.X * sin + point.Y * cos);
        }
    }
}
