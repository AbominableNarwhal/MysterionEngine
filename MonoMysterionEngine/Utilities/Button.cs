﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine.Systems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class Button
    {
        public enum Events { Click, Cancel }
        private event EventHandler<ButtonClickEventArgs> ButtonClickEvent;
        private Texture2D normalTexture;
        private Texture2D pressedTexture;
        private bool pressed = false;
        private bool hovering = false;

        public Button(Texture2D normalTexture, Texture2D pressedTexture, SpriteFont font, RelativeTransform transform)
        {
            SpriteImage = new Sprite();
            NormalTexture = normalTexture;
            PressedTexture = pressedTexture;
            Bounds = new RotatedRectangle(transform, 0, 0, SpriteImage.Width, SpriteImage.Height);
            TextFont = new Font(font);
        }
        public Sprite SpriteImage
        { get; private set; }
        public Font TextFont
        { get; private set; }
        public Texture2D NormalTexture
        {
            get
            { return normalTexture; }
            set
            {
                normalTexture = value;
                UpdateSprite();
            }
        }
        public Texture2D PressedTexture
        {
            get
            { return pressedTexture; }
            set
            {
                pressedTexture = value;
                UpdateSprite();
            }
        }

        public bool Pressed
        {
            get
            { return pressed; }
            private set
            {
                pressed = value;
                UpdateSprite();
            }
        }
        public RotatedRectangle Bounds
        { get; set; }

        private void UpdateSprite()
        {
            if (Pressed)
            {
                SpriteImage.Texture = PressedTexture;
            }
            else
            {
                SpriteImage.Texture = NormalTexture;
            }
        }
        public void Subscribe(EventHandler<ButtonClickEventArgs> handler)
        {
            ButtonClickEvent += handler;
        }

        public void OnInputUpdate(KeyboardState keyboardState, MouseState mouseState)
        {
            if (Bounds.ContainsPoint(new Vector2(mouseState.X, mouseState.Y)))
            {
                if (mouseState.LeftButton == ButtonState.Released)
                {
                    hovering = true;
                    if (Pressed)
                    {
                        ButtonClickEvent?.Invoke(this, new ButtonClickEventArgs() { EventType = Events.Click });
                        Pressed = false;
                    }
                }
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    if (hovering)
                    {
                        Pressed = true;
                    }
                    hovering = false;
                }
            }
            else
            {
                //Revert state
                if (Pressed)
                {
                    ButtonClickEvent?.Invoke(this, new ButtonClickEventArgs() { EventType = Events.Cancel });
                    Pressed = false;
                }
                hovering = false;
            }
        }
        public void OnDraw(RenderSystem renderSystem)
        {
            renderSystem.RenderBegin();
            renderSystem.RenderSprite(SpriteImage, new Vector2(0, 0), new Vector2(1, 1), 0);
            // Render text
            if (TextFont.Text != "")
            {
                int x = SpriteImage.Width / 2 - TextFont.Width / 2;
                int y = SpriteImage.Height / 2 - TextFont.Height / 2;
                renderSystem.RenderText(TextFont, new Vector2(x, y), new Vector2(1, 1), 0);
            }
            renderSystem.RenderEnd();
        }

        public class ButtonClickEventArgs
        {
            public Events EventType { get; set; }
        }
    }
}
