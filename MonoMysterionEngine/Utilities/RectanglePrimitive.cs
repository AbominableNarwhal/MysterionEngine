﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class RectanglePrimitive
    {
        public static PrimitiveGeometry GetGeometry(int width, int height, Color color)
        {
            VertexPositionColor[] vertices = {
                new VertexPositionColor(new Vector3(0, 0, 0), color),
                new VertexPositionColor(new Vector3(width, 0, 0), color),
                new VertexPositionColor(new Vector3(width, height, 0), color),
                new VertexPositionColor(new Vector3(0, height, 0), color),

            };
            short[] indices = { 0, 1, 3, 1, 2, 3 };

            return new PrimitiveGeometry { vertices = vertices, indices = indices, TriangleCount = 2};
        }
    }
}
