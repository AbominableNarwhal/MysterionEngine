﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class SDFTextLayout
    {
        public delegate void AlignmentFunc(List<Vector2> renderData, string text, int lineIndex, int lineCharNum, int lineWidth, Rectangle bounds);
        private AlignmentFunc alignmentFunc;
        private string currentWord;
        private List<Vector2> renderData;
        private string text;
        private SDFFont font;
        private int cursorPos;
        private char lastChar;
        private int wordIndex;
        private int index;
        private int numOfLines;
        private Rectangle textBounds;
        private float scale;
        private int curCharLineWidth;
        private int curWordLineWidth;
        private int lineIndex;
        public SDFTextLayout()
        {
            renderData = new List<Vector2>();
        }
        public IEnumerable<Vector2> RenderData
        {
            get
            {
                return renderData;
            }
        }
        public void BeginIteration(string text, SDFFont font, Rectangle textBounds, float scale, AlignmentFunc alignmentFunc)
        {
            this.text = text;
            this.font = font;
            this.textBounds = textBounds;
            wordIndex = 0;
            index = 0;
            cursorPos = 0;
            lastChar = (char)0;
            numOfLines = 0;
            currentWord = "";
            this.scale = scale;
            curCharLineWidth = 0;
            curWordLineWidth = 0;
            lineIndex = 0;
            this.alignmentFunc = alignmentFunc;
            if (renderData.Capacity < text.Length)
            {
                renderData.Capacity = text.Length;
                renderData.AddRange(Enumerable.Repeat(new Vector2(), text.Length));
            }
        }
        public void EndIteration()
        {
            if (currentWord != "")
            {
                ProcessWord(currentWord);
                wordIndex = index;
                AddNewLine();
            }
        }
        public void ProcessChar(char character)
        {
            currentWord += character;
            if (character == ' ')
            {
                ProcessWord(currentWord);
                wordIndex = index + 1;
            }
            index += 1;
        }
        private void ProcessWord(string word)
        {
            if (!Measure(word))
            {
                AddNewLine();
                lineIndex = wordIndex;
                Measure(word);
            }
            curWordLineWidth = curCharLineWidth;
            currentWord = "";
        }
        private bool Measure(string word)
        {
            int charIndex = wordIndex;
            foreach (char character in word)
            {
                if (!Measure(character, charIndex))
                {
                    return false;
                }
                charIndex += 1;
            }
            return true;
        }
        private bool Measure(char character, int charIndex)
        {
            int kerningAmount = 0;
            SDFGlyph glyph = font.Glyphs[character];
            int xOffset = glyph.xOffset;
            if (lastChar != 0)
            {
                if (font.Kernings.ContainsKey((lastChar, character)))
                {
                    kerningAmount = font.Kernings[(lastChar, character)].amount;
                }
            }
            else
            {
                xOffset = 0;
            }

            int LeftAndRightPadding = font.PaddingLeft + font.PaddingRight;
            if (character != ' ')
            {
                Vector2 pos = new Vector2(cursorPos + xOffset /*- font.PaddingLeft*/ + kerningAmount,
                    glyph.yOffset /*- font.PaddingTop*/ + font.Base * numOfLines) * scale;
                renderData[charIndex] = pos;
                int lineWidth = (int)(pos.X + glyph.width * scale);
                if (lineWidth > textBounds.Width)
                {
                    return false;
                }
                curCharLineWidth = lineWidth;
            }
            cursorPos += glyph.xAdvance - LeftAndRightPadding;
            lastChar = character;
            return true;
        }

        private void AddNewLine()
        {
            alignmentFunc?.Invoke(renderData, text, lineIndex, wordIndex - lineIndex, curWordLineWidth, textBounds);
            cursorPos = 0;
            numOfLines += 1;
            lastChar = (char)0;
            curCharLineWidth = 0;
            curWordLineWidth = 0;
        }
    }
}
