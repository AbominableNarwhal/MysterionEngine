﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using MonoMysterionEngine.Systems;

namespace MonoMysterionEngine.Utilities
{
    public class Sprite : Drawable
    { 
        private Texture2D texture;
        public Rectangle SourceRectangle { get; set; }
        public int Width
        { get { return SourceRectangle.Width; } }
        public int Height
        { get { return SourceRectangle.Height; } }

        public Sprite()
        {
            SourceRectangle = new Rectangle(0, 0, 0, 0);
        }
        public Sprite(Sprite sprite) : base(sprite)
        {
            texture = sprite.Texture;
            SourceRectangle = sprite.SourceRectangle;
        }
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
                if(texture != null)
                    SourceRectangle = new Rectangle(SourceRectangle.X, SourceRectangle.Y, value.Width, value.Height);
            }
        }
            
        public bool LoadImage(string name)
        {
            Texture = RenderSystem.Renderer.LoadTexture(name);
            return Texture != null;
        }
        public void SetDimentions(int width, int height)
        {
            SourceRectangle.Inflate(width, height);
        }
        public void SetSourcePosition(int x, int y)
        {
            SourceRectangle.Offset(x, y);
        }

    }
}
