﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class SDFFont
    {
        public Texture2D FontTexture { get; set; }
        public Dictionary<int, SDFGlyph> Glyphs { get; set; }
        public Dictionary<ValueTuple<int, int>, SDFKerning> Kernings { get; set; }
        public int PaddingLeft { get; set; }
        public int PaddingRight { get; set; }
        public int PaddingTop { get; set; }
        public int PaddingBottom { get; set; }
        public int LineHeight { get; set; }
        public int Base { get; set; }
    }
    public struct SDFGlyph
    {
        public int id;
        public int x;
        public int y;
        public int width;
        public int height;
        public int xOffset;
        public int yOffset;
        public int xAdvance;
    }
    public struct SDFKerning
    {
        public int first;
        public int second;
        public int amount;
    }
}
