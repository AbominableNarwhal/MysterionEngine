﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class SDFText : Drawable
    {
        public SDFFont Font { get; }
        public string Text { get; set; }
        public int Size { get; set; }
        public float Scale
        {
            get
            { return Size / (float)Font.Base; }
        }
        private SDFTextLayout layout;
        private Rectangle textBounds;

        public int Width
        {
            get
            {
                return textBounds.Width;
            }
            set
            {
                textBounds.Width = value;
            }
        }
        public int Height
        {
            get
            {
                return textBounds.Height;
            }
            set
            {
                textBounds.Height = value;
            }
        }

        public SDFText(SDFFont _sdfFont)
        {
            Font = _sdfFont;
            layout = new SDFTextLayout();
            Width = int.MaxValue;
            Height = int.MaxValue;
            Size = Font.Base;
            Text = "";
        }
        public SDFText(SDFText other)
        {
            Font = other.Font;
            layout = new SDFTextLayout();
            Width = other.Width;
            Height = other.Height;
            Size = other.Size;
            Text = other.Text;
        }

        public IEnumerable<Vector2> GetTextLayout()
        {
            layout.BeginIteration(Text, Font, textBounds, Scale, AlignmentFunctions.CenterAlign);

            foreach (char character in Text)
            {
                layout.ProcessChar(character);
            }

            layout.EndIteration();
            return layout.RenderData;
        }
        private static class AlignmentFunctions
        {
            //Left align is the default implementation

            public static void RightAlign(List<Vector2> renderData, string text, int lineIndex, int lineCharNum, int lineWidth, Rectangle bounds)
            {
                int linespace = bounds.Width - lineWidth;
                for (int i = lineIndex; i < lineIndex + lineCharNum; i++)
                {
                    renderData[i] = new Vector2(renderData[i].X + linespace, renderData[i].Y);
                }
            }

            public static void CenterAlign(List<Vector2> renderData, string text, int lineIndex, int lineCharNum, int lineWidth, Rectangle bounds)
            {
                int linespace = (int)((bounds.Width - lineWidth) / 2);
                for (int i = lineIndex; i < lineIndex + lineCharNum; i++)
                {
                    renderData[i] = new Vector2(renderData[i].X + linespace, renderData[i].Y);
                }
            }
        }
    }
}
