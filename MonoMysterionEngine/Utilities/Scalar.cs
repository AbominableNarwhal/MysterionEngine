﻿
namespace MonoMysterionEngine.Utilities
{
    public class Scalar
    {
        public double Value { get; set; }
        public Scalar()
        {
            Value = 0;
        }
        public Scalar(double val)
        {
            Value = val;
        }
    }
}
