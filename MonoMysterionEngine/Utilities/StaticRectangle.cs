﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class StaticRectangle
    {
        public Rectangle rectangle;
        public StaticRectangle()
        {
            rectangle = new Rectangle();
        }
        public StaticRectangle(int x, int y, int width, int height)
        {
            rectangle = new Rectangle(x, y, width, height);
        }
        public void Set(int x, int y, int width, int height)
        {
            rectangle.X = x;
            rectangle.Y = y;
            rectangle.Width = width;
            rectangle.Height = height;
        }
    }
}
