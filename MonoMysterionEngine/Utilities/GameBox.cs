﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MonoMysterionEngine.Utilities
{
    public class GameBox
    {
        RotatedRectangle box;
        List<GameBox> collisionPartners;

        public GameBox(int x, int y, int width, int height, Vector2D relativeVector)
        {
            box = new RotatedRectangle(null, 0.0f, 0, 0, 0);
            collisionPartners = new List<GameBox>();
        }
        public GameBox(int x, int y, int width, int height) : this(x, y, width, height, new Vector2D())
        {}
        public RotatedRectangle BoundingBox
        {
            get { return box; }
        }
        public bool Collision(GameBox other)
        {
            return true;// box.Intersects(other.BoundingBox);
        }
        public bool HasPartner(GameBox parner)
        {
            return collisionPartners.Contains(parner);
        }
        public void RemovePartner(GameBox partner)
        {
            collisionPartners.Remove(partner);
        }
        public void separatePartner(GameBox partner)
        {
            collisionPartners.Remove(partner);
            partner.RemovePartner(this);
        }
        public void separatePartners()
        {
            foreach (var partner in collisionPartners)
            {
                partner.RemovePartner(this);
            }
            collisionPartners.Clear();
        }
        public void addPartner(GameBox partner)
        {
            collisionPartners.Add(partner);
        }
    }
}
