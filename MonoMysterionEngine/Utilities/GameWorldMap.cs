﻿using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class GameWorldMap
    {
        public Dictionary<long, Texture2D> tileSets;
        public List<IObjectGroup> Objects { get; set; }
        public GameWorldMap()
        {
            tileSets = new Dictionary<long, Texture2D>();
            Objects = new List<IObjectGroup>();
        }
    }
}
