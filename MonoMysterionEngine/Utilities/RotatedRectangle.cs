﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class RotatedRectangle
    {
        public RelativeTransform transform;
        public Vector2 tl;
        public Vector2 br;
        public float Rotation { get; set; }
        public RotatedRectangle(RelativeTransform _transform, float ox, float oy, float width, float height)
        {
            transform = _transform;
            tl = new Vector2(ox, oy);
            br = new Vector2(ox + width, oy + height);
        }
        public RotatedRectangle(RelativeTransform _transform) : this(_transform, 0, 0, 0, 0)
        { }
        public float Width
        {
            get
            {
                return br.X - tl.X;
            }
            set
            {
                br.X = tl.X + value;
            }
        }
        public float Height
        {
            get
            {
                return br.Y - tl.Y;
            }
            set
            {
                br.Y = tl.Y + value;
            }
        }
        public Vector2 TopLeft
        {
            get
            {
                return Vector2.Transform(tl, transform.Transform);
                //return new Vector2(tl.X + transform.Position.X, tl.X + transform.Position.Y);
            }
        }
        public Vector2 TopRight
        {
            get
            {
                Vector2 tr = new Vector2(br.X, tl.Y);
                tr = Engine.RotateVector(tr, Rotation);
                return Vector2.Transform(tr, transform.Transform);
            }
        }
        public Vector2 BottomLeft
        {
            get
            {
                Vector2 bl = new Vector2(tl.X, br.Y);
                bl = Engine.RotateVector(bl, Rotation);
                return Vector2.Transform(bl, transform.Transform);
            }
        }
        public Vector2 BottomRight
        {
            get
            {
                Vector2 temp = Engine.RotateVector(br, Rotation);
                return Vector2.Transform(temp, transform.Transform);
            }
        }
        public bool ContainsPoint(Vector2 point)
        {
            Vector2 topLeft = TopLeft;
            Vector2 topRight = TopRight;
            Vector2 bottomRight = BottomRight;
            Vector2 axis1 = topRight - topLeft;
            Vector2 axis2 = topRight - bottomRight;

            //axis test 1
            float scalar1 = Vector2.Dot(topLeft, axis1);
            float scalar2 = Vector2.Dot(topRight, axis1);
            float scalar3 = Vector2.Dot(point, axis1);
            float maxScalar = Math.Max(scalar1, scalar2);
            float minScalar = Math.Min(scalar1, scalar2);
            if (minScalar > scalar3 || maxScalar < scalar3)
            {
                return false;
            }
            //axis test 2
            scalar1 = Vector2.Dot(topRight, axis2);
            scalar2 = Vector2.Dot(bottomRight, axis2);
            scalar3 = Vector2.Dot(point, axis2);
            maxScalar = Math.Max(scalar1, scalar2);
            minScalar = Math.Min(scalar1, scalar2);
            if (minScalar > scalar3 || maxScalar < scalar3)
            {
                return false;
            }
            return true;
        }
    }
}
