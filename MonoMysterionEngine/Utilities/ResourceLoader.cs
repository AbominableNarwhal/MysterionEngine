﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class ResourceLoader
    {
        public static T Load<T>(string fileName)
        {
            return MysterionEngineGame.Game.Content.Load<T>(fileName);
        }
    }
}
