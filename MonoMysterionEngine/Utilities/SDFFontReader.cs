﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class SDFFontReader : ContentTypeReader<SDFFont>
    {
        protected override SDFFont Read(ContentReader input, SDFFont existingInstance)
        {
            SDFFont font = new SDFFont();
            font.Glyphs = new Dictionary<int, SDFGlyph>();
            font.Kernings = new Dictionary<ValueTuple<int, int>, SDFKerning>();
            string fontTextureFile = input.ReadString();
            font.FontTexture = input.ContentManager.Load<Texture2D>(fontTextureFile);
            font.PaddingLeft = input.ReadInt32();
            font.PaddingRight = input.ReadInt32();
            font.PaddingTop = input.ReadInt32();
            font.PaddingBottom = input.ReadInt32();
            font.LineHeight = input.ReadInt32();
            font.Base = input.ReadInt32();

            int numOfGlyphs = input.ReadInt32();
            for (int i = 0; i < numOfGlyphs; i++)
            {
                SDFGlyph glyph = new SDFGlyph
                {
                    id = input.ReadInt32(),
                    x = input.ReadInt32(),
                    y = input.ReadInt32(),
                    width = input.ReadInt32(),
                    height = input.ReadInt32(),
                    xOffset = input.ReadInt32(),
                    yOffset = input.ReadInt32(),
                    xAdvance = input.ReadInt32()
                };
                font.Glyphs.Add(glyph.id, glyph);
            }

            int numOfKernings = input.ReadInt32();
            for (int i = 0; i < numOfKernings; i++)
            {
                SDFKerning kerning = new SDFKerning
                {
                    first = input.ReadInt32(),
                    second = input.ReadInt32(),
                    amount = input.ReadInt32()
                };
                font.Kernings.Add((kerning.first, kerning.second), kerning);
            }
            return font;
        }
    }
}
