﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public abstract class Drawable
    {
        public Drawable()
        {
            Color = new Color(Color.White, 255);
            Center = new Vector2D(0, 0);
        }
        public Drawable(Drawable drawable)
        {
            Color = drawable.Color;
            Center = drawable.Center;
            IsHFlipped = drawable.IsHFlipped;
            IsVFlipped = drawable.IsVFlipped;
        }
        public Color Color { get; set; }
        public Vector2D Center { get; set; }
        public bool IsHFlipped { get; set; }
        public bool IsVFlipped { get; set; }
        public SpriteEffects Effect()
        {
            if (IsVFlipped == true && IsHFlipped == true)
            {
                return SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically;
            }
            else if (IsHFlipped == true)
            {
                return SpriteEffects.FlipHorizontally;
            }
            else if (IsVFlipped == true)
            {
                return SpriteEffects.FlipVertically;
            }
            else
            {
                return SpriteEffects.None;
            }
        }
    }
}
