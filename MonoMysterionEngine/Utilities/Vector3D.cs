﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class Vector3D
    {
        public Vector3 vector;
        public Vector3D()
        {
            vector = new Vector3();
        }
        public Vector3D(Vector3D vector)
        {
            this.vector = new Vector3(vector.vector.X, vector.vector.Y, vector.vector.Z);
        }
        public Vector3D(float x, float y, float z)
        {
            vector = new Vector3(x, y, z);
        }
        public void Set(float x, float y, float z)
        {
            vector.X = x;
            vector.Y = y;
            vector.Z = z;
        }
        public void Set(Vector3 vec)
        {
            vector.X = vec.X;
            vector.Y = vec.Y;
            vector.Z = vec.Z;
        }
    }
}
