﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoMysterionEngine.Utilities
{
    public class Font : Drawable
    {
        private string text;
        private Vector2 sizeVec;
        public SpriteFont spriteFont;

        public Font(SpriteFont spriteFont)
        {
            this.spriteFont = spriteFont;
            text = "";
        }
        public Font(Font font) : base(font)
        {
            text = font.text;
            sizeVec = font.sizeVec;
            spriteFont = font.spriteFont;
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
        public int Width
        {
            get
            {
                sizeVec = spriteFont.MeasureString(text);
                return (int)sizeVec.X;
            }
        }
        public int Height
        {
            get
            {
                sizeVec = spriteFont.MeasureString(text);
                return (int)sizeVec.Y;
            }
        }
    }   
}
