﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public struct PrimitiveGeometry
    {
        public VertexPositionColor[] vertices;
        public short[] indices;
        public int TriangleCount { get; set; }
    }
}
