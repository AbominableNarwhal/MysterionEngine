﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class RelativeTransform
    {
        public RelativeTransform()
        {
            Scale = new Vector3(1, 1, 1);
        }
        public RelativeTransform(RelativeTransform transform)
        {
            Scale = transform.Scale;
            Position = transform.Position;
            Rotation = transform.Rotation;
            Parent = transform.Parent;
        }
        public Matrix Transform
        {
            get
            {
                if (Parent == null)
                {
                    return LocalTransform;
                }
                return  Matrix.CreateRotationZ(Rotation) * Matrix.CreateScale(Scale) * Matrix.CreateTranslation(Position)* Parent.Transform;
            }
        }
        public Matrix LocalTransform
        {
            get
            {
                return Matrix.CreateRotationZ(Rotation) * Matrix.CreateScale(Scale) * Matrix.CreateTranslation(Position);
            }
        }
        public Vector3 Position;
        public float Rotation;
        public Vector3 Scale;
        public RelativeTransform Parent
        { get; set; }
    }
}
