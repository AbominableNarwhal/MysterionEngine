﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class AnimSprite : Sprite
    {
		private int animColumns;
	    private	double currentFrame;
		private int totalFrames;
		private int animdir;
		private int fps;

		public AnimSprite(){
            animColumns = 0;
		    currentFrame = 0;
		    totalFrames = 0;
		    animdir = 1;
		    fps = 30;
        }

		bool animate(int deltaTime){
            bool animLooped = false;
		    double secondFrac = (double)deltaTime / 1000;
	    	currentFrame += fps*secondFrac*animdir;
	    	if (currentFrame > totalFrames || currentFrame < 0)
	    	{
		    	animLooped = true;
	    		//need to do modulus with decimals
		    	double deci = currentFrame - (int)currentFrame;
	    		currentFrame = (int)currentFrame % totalFrames;
		    	currentFrame += deci;
	    	}
	    	cutFrame();
	    	return animLooped;
        }

        public void cutFrame(){
            //set the source rectangle
		    int fx = ((int)currentFrame % animColumns) * Width;
		    int fy = ((int)currentFrame / animColumns) * Height;

		    SetSourcePosition(fx, fy);
        }

		public int getAnimColumns(){
            return animColumns;
        }

		public double getCurrentFrame(){
            return currentFrame;
        }

		public int getTotalFrames(){
            return totalFrames;
        }

		public int getAnimDir(){
            return animdir;
        }

		public int getFps(){
            return fps;
        }

		public void setAnimColumns(int val){
            animColumns = val;
        }
		
        public void setCurrentFrame(double val){
            currentFrame = val;
        }
		
        public void setTotalFrames(int val){
            totalFrames = val;
        }

		public void setAnimDir(int val){
            animdir = val;
        }
		
        public void setFps(int val){
            fps = val;
        }

    }
}
