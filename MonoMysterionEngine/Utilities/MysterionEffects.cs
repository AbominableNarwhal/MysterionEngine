﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    internal class MysterionEffects
    {
        const string SDFFontEffectName = "MonoMysterionEngine.Content.sdf-shader.mgfxo";
        public static readonly MysterionEffects SDFFontEffect = new MysterionEffects(SDFFontEffectName);

        private readonly object _locker = new object();
        private readonly string _name;
        private volatile byte[] _bytecode;

        private MysterionEffects(string name)
        {
            _name = name;
        }

        public byte[] Bytecode
        {
            get
            {
                if (_bytecode == null)
                {
                    lock (_locker)
                    {
                        if (_bytecode != null)
                            return _bytecode;

                        var assembly = typeof(MysterionEffects).GetTypeInfo().Assembly;

                        var stream = assembly.GetManifestResourceStream(_name);
                        using (var ms = new MemoryStream())
                        {
                            stream.CopyTo(ms);
                            _bytecode = ms.ToArray();
                        }
                    }
                }

                return _bytecode;
            }
        }
    }
}
