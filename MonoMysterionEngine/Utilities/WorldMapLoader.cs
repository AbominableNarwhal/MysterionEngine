﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MonoMysterionEngine.Utilities
{
    public interface IMapLoader
    {
        IObjectGroup NewObjectGroup(string name, long id, float x, float y);
        void NewObjectData(IObjectGroup group, string name, float x, float y, float width, float height, float rotation,
            Texture2D texture, TextData? textData, Dictionary<string, string> properties);
    }
    public interface IObjectGroup
    {
        string Name { get; set; }
        long Id { get; set; }
    }
    public struct TextData
    {
        public Font font;
        public int pixelSize;
    }
    public class DefaultMapLoader : IMapLoader
    {
        public virtual IObjectGroup NewObjectGroup(string name, long id, float x, float y)
        {
            GameObject gameObjectGroup = new GameObject();
            gameObjectGroup.Name = name;
            gameObjectGroup.Id = id;
            gameObjectGroup.Transforms.Position = new Vector3(x, y, 0);
            return gameObjectGroup;
        }
        public virtual void NewObjectData(IObjectGroup group, string name, float x, float y, float width, float height, float rotation,
            Texture2D texture, TextData? textData, Dictionary<string, string> properties)
        {
            if (!(group is GameObject))
            {
                return;
            }

            GameObject gameObjectGroup = (GameObject)group;
            GameObject gameObject = new GameObject();
            LoadSpriteRenderData(gameObject, width, height, out Vector2 scale, texture);
            LoadTextRenderData(gameObject, width, height, out scale, textData);
            LoadGameObjectData(gameObject, name, x, y, rotation, scale);

            gameObjectGroup.AddChild(gameObject);
        }
        protected void LoadTextRenderData(GameObject gameObject, float width, float height, out Vector2 scale, TextData? textData)
        {
            LoadTextRenderData(gameObject, out scale, textData, out TextRenderComponent renderComponent);
            if (renderComponent != null)
            {
                gameObject.AddComponent(renderComponent);
            }
        }
        protected void LoadTextRenderData(GameObject gameObject, out Vector2 scale,
            TextData? textData, out TextRenderComponent renderComponent)
        {
            scale = new Vector2 { X = 1, Y = 1 };
            renderComponent = null;
            if (textData.HasValue)
            {
                renderComponent = new TextRenderComponent();
                renderComponent.font = textData.Value.font;
                float currentHeight = renderComponent.font.Height;
                float targetHeight = textData.Value.pixelSize;
                float finalScale = targetHeight / currentHeight;
                scale = new Vector2(finalScale, finalScale);
            }
        }
        protected void LoadSpriteRenderData(GameObject gameObject, float width, float height, out Vector2 scale, Texture2D texture)
        {
            LoadSpriteRenderData(gameObject, width, height, out scale, texture, out SpriteRenderComponent renderComponent);
            if (renderComponent != null)
            {
                gameObject.AddComponent(renderComponent);
            }
        }
        protected void LoadSpriteRenderData(GameObject gameObject, float width, float height, out Vector2 scale,
            Texture2D texture, out SpriteRenderComponent renderComponent)
        {
            scale = new Vector2 { X = 1, Y = 1 };
            renderComponent = null;
            if (texture != null)
            {
                scale = new Vector2((float)width / texture.Width, (float)height / texture.Height);
                Sprite sprite = new Sprite();
                sprite.Texture = texture;
                renderComponent = new SpriteRenderComponent()
                { SpriteImage = sprite };
            }
        }
        protected void LoadGameObjectData(GameObject gameObject, string name, float x, float y, float rotation, Vector2 scale)
        {
            gameObject.Name = name;
            gameObject.Transforms.Position = new Vector3(x, y, 0);
            gameObject.Transforms.Scale = new Vector3(scale, 0);
            gameObject.Transforms.Rotation = MathHelper.ToRadians(rotation);
        }
    }
    public class WorldMapLoader
    {
        public static GameWorldMap LoadMap(string filename, IMapLoader mapLoader = null)
        {
            if (mapLoader == null)
            {
                mapLoader = new DefaultMapLoader();
            }
            XElement mapFile;
            try
            {
                mapFile = XElement.Load(filename);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return null;
            }

            GameWorldMap map = new GameWorldMap();

            IEnumerable<XElement> layers = mapFile.Elements();
            foreach (XElement layer in layers)
            {
                if (layer.Name == "objectgroup")
                {
                    string groupName = layer.Attribute("name").Value;
                    long.TryParse(layer.Attribute("id").Value, out long groupId);
                    float.TryParse(GetAttribute(layer, "offsetx"), out float groupx);
                    float.TryParse(GetAttribute(layer, "offsety"), out float groupy);
                    IObjectGroup parentObject = mapLoader.NewObjectGroup(groupName, groupId, groupx, groupy);
                    if (parentObject == null)
                    {
                        continue;
                    }
                    map.Objects.Add(parentObject);

                    IEnumerable<XElement> groupData = layer.Elements();
                    foreach (XElement objectData in groupData)
                    {
                        //Set attributes with default values
                        Texture2D texture = null;
                        float x = 0;
                        float y = 0;
                        float width = 0;
                        float height = 0;
                        float rotation = 0;
                        float orginAdjust = 0;
                        string name = GetAttribute(objectData, "name");
                        //Getting the texture for this object
                        if (long.TryParse(GetAttribute(objectData, "gid"), out long gid))
                        {
                            texture = map.tileSets[gid];
                            //Right now the Tiled Map Editor make objects with sprite of an origin 
                            //on the bottom left corner so the coordinates need to been adjusted to
                            //be on the top left corner
                            orginAdjust = texture.Height;
                        }
                        float.TryParse(GetAttribute(objectData, "x"), out x);
                        float.TryParse(GetAttribute(objectData, "y"), out y);
                        float.TryParse(GetAttribute(objectData, "width"), out width);
                        float.TryParse(GetAttribute(objectData, "height"), out height);
                        float.TryParse(GetAttribute(objectData, "rotation"), out rotation);

                        mapLoader.NewObjectData(parentObject, name, x, y - orginAdjust, width, height, rotation, texture,
                            GetText(objectData), GetProperties(objectData));
                    }
                }
                else if (layer.Name == "tileset")
                {
                    long.TryParse(layer.Attribute("firstgid").Value, out long id);
                    XElement image = layer.Element("image");
                    string source = RemoveFileExtention(image.Attribute("source").Value);
                    map.tileSets.Add(id, ResourceLoader.Load<Texture2D>(source));
                }
            }
            return map;
        }
        private static Dictionary<string, string> GetProperties(XElement element)
        {
            XElement propertyTag = element.Element("properties");
            if (propertyTag == null)
            {
                return null;
            }

            Dictionary<string, string> propDict = new Dictionary<string, string>();
            IEnumerable<XElement> properties = propertyTag.Elements();
            foreach (XElement property in properties)
            {
                string name = GetAttribute(property, "name");
                string value = GetAttribute(property, "value");
                propDict.Add(name, value);
            }
            return propDict;
        }
        private static TextData? GetText(XElement element)
        {
            XElement text = element.Element("text");
            if (text == null)
            {
                return null;
            }

            SpriteFont spriteFont = ResourceLoader.Load<SpriteFont>(GetAttribute(text, "fontfamily"));
            Font font = new Font(spriteFont);
            font.Text = text.Value;
            font.Color = StringToColor(GetAttribute(text, "color", "#000000").Substring(1));
            int.TryParse(GetAttribute(text, "pixelsize", "16"), out int pixelSize);
            TextData textData = new TextData{font = font, pixelSize = pixelSize };
            return textData;
        }
        private static Color StringToColor(string hex)
        {
            byte[] colorBytes = Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();

            if (colorBytes.Length == 3)
            {
                colorBytes = new byte[] { colorBytes[0], colorBytes[1], colorBytes[2], 255 };
            }
            else if (colorBytes.Length == 4)
            {
                colorBytes = new byte[] { colorBytes[1], colorBytes[2], colorBytes[3], colorBytes[0] };
            }
            else
            {
                return new Color();
            }

            return new Color() { R = colorBytes[0], G = colorBytes[1], B = colorBytes[2], A = colorBytes[3] };
        }
        private static string GetAttribute(XElement element, string name, string defualt = "")
        {
            XAttribute attribute = element.Attribute(name);
            return attribute == null ? defualt : attribute.Value;
        }
        private static string RemoveFileExtention(string filename)
        {
            return filename.Substring(0, filename.LastIndexOf('.'));
        }
    }
}
