﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MonoMysterionEngine.Utilities
{
    public class Vector2D
    {
        public Vector2 vector;
        public Vector2D()
        {
            vector = new Vector2();
        }
        public Vector2D(float x, float y)
        {
            vector = new Vector2(x, y);
        }
        public void Set(float x, float y)
        {
            vector.X = x;
            vector.Y = y;
        }
        public void Set(Vector2 vec)
        {
            vector.X = vec.X;
            vector.Y = vec.Y;
        }
    }
}
