﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Utilities
{
    public class SDFFontEffect : Effect
    {
        private enum SDFFontParams { TextColor = 1 }
        private SDFFontParams dirtyParams;
        private EffectParameter textColorParam;

        private Color textColor;
        public Color TextColor
        {
            get { return textColor; }
            set
            {
                textColor = value;
                dirtyParams |= SDFFontParams.TextColor;
            }
        }

        public SDFFontEffect(GraphicsDevice device) :
            base(device, MysterionEffects.SDFFontEffect.Bytecode)
        {
            textColorParam = Parameters["TextColor"];
            CurrentTechnique = Techniques["SDFFontRender"];
            TextColor = new Color(255, 255, 255, 255);
        }
        
        protected override void OnApply()
        {
            if ((dirtyParams & SDFFontParams.TextColor) != 0)
            {
                textColorParam.SetValue(TextColor.ToVector4());
            }
        }
    }
}
