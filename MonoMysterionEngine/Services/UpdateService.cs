﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Services
{
    public class UpdateService : Service
    {
        public UpdateService()
        {
            Tag = "Update";
        }
        public void PerformUpdateService(GameTime gameTime)
        {
            foreach (UpdateComponent comp in Components)
            {
                comp.Update(gameTime);
            }
        }
    }
}
