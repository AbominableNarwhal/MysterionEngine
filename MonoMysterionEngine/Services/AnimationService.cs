﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoMysterionEngine.Components;

namespace MonoMysterionEngine.Services
{
    public class AnimationService : Service
    {
        public AnimationService()
        {
            Tag = "Animate";
        }

        public void PerformAnimationService(GameTime gameTime)
        {
            for(int i = Components.Count() - 1; i >= 0; i--)
            {
                AnimationComponent comp = (AnimationComponent)Components[i];
                comp.Animate(gameTime);
                if (comp.Progress == 1)
                {
                    comp.Complete();
                    //comp.Owner.RemoveComponent(comp);
                }
            }
        }
    }
}
