﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Components;

namespace MonoMysterionEngine.Services
{
    public class ServiceManager
    {
        Dictionary<string, Service> services;
        public ServiceManager()
        {
            services = new Dictionary<string, Service>();
        }
        public void AddComponent(Component comp)
        {
            if (comp.Tag != "default")
            {
                services[comp.Tag].AddComponent(comp);
            }
        }
        public void AddComponents(IEnumerable<Component> comps)
        {
            foreach (var comp in comps)
            {
                AddComponent(comp);
            }
        }
        public void RemoveComponent(Component comp)
        {
            if (comp.Tag != "default")
            {
                services[comp.Tag].RemoveComponent(comp);
            }
        }
        public void RemoveComponents(IEnumerable<Component> comps)
        {
            foreach (var comp in comps)
            {
                RemoveComponent(comp);
            }
        }
        public void AddService(Service service)
        {
            services.Add(service.Tag, service);
        }
        public void RemoveService(Service service)
        {
            services.Remove(service.Tag);
        }
    }
}
