﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Systems;

namespace MonoMysterionEngine.Services
{
    public class RenderService : Service
    {
        public RenderService()
        {
            Tag = "Render";
        }
        public void PerformRenderService()
        {
            foreach (RenderComponent comp in Components)
            {
                if (comp.Visable)
                {
                    RenderSystem.Renderer.GlobalTransform = (comp.Transforms == null) ? Matrix.Identity : comp.Transforms.Transform;
                    RenderSystem.Renderer.CurrentEffect = comp.RenderEffect;
                    comp.Draw(RenderSystem.Renderer);
                }
            }
        }
    }
}
