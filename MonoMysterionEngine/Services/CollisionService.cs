﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Components;

namespace MonoMysterionEngine.Services
{
    public class CollisionService : Service
    {
        public CollisionService()
        {
            Tag = "Collision";
        }
        public void performCollisionService()
        {
            for (int i = 0; i < Components.Count-1; i++)
            {
                CollisionComponent component1 = (CollisionComponent)Components[i];
                for (int c = i + 1; c < Components.Count; c++)
                {
                    CollisionComponent component2 = (CollisionComponent)Components[c];
                    component1.DetectCollision(component2);
                }
            }
        }
    }
}
