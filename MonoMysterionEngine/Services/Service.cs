﻿using System.Collections.Generic;
using MonoMysterionEngine.Components;

namespace MonoMysterionEngine.Services
{
    public class Service
    {
        public Service()
        {
            Tag = "default";
            Components = new List<Component>();
        }
        ~Service()
        {
            Components.Clear();
        }
        public string Tag { get; protected set; }
        public List<Component> Components { get; }
        public void AddComponent(Component comp)
        {
            Components.Add(comp);
        }
        public void RemoveComponent(Component comp)
        {
            Components.Remove(comp);
        }

    }
}
