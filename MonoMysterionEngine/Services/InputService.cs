﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Systems;

namespace MonoMysterionEngine.Services
{
    public class InputService : Service
    {
        public MouseState MouseUpdate { get; set; }
        public KeyboardState KeyboardUpdate { get; set; }
        public InputService()
        {
            Tag = "Input";
        }
        public void performInputService()
        {
            InputSystem.Input.GrabInput();
            foreach (InputComponent comp in Components)
            {
                comp.InputStateUpdate(InputSystem.Input.KeyboardState, InputSystem.Input.MouseState);
            }
        }
    }
}
