﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Objects
{
    public class RectangleObject : GameObject
    {
        private PrimitiveRenderComponent renderComponent;
        public RectangleObject(int width, int height, Color color)
        {
            renderComponent = new PrimitiveRenderComponent()
            {
                Geometry = RectanglePrimitive.GetGeometry(width, height, color)
            };
            Width = width;
            Height = height;
            AddComponent(renderComponent);
        }

        public int Width { get; private set; }
        public int Height { get; private set; }
    }
}
