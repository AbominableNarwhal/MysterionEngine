﻿using System.Collections.Generic;
using System.Linq;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Objects
{
    public class GameObject : IObjectGroup
    {
        private Vector3D velocity;
        private Vector3D acceleration;
        private ulong componentID;
        private List<GameObject> children;

        public GameObject()
        {
            velocity = new Vector3D();
            acceleration = new Vector3D();
            Active = false;
            componentMap = new Dictionary<string, Component>();
            Name = "";
            Transforms = new RelativeTransform();
            children = new List<GameObject>();
        }
        public GameObject(GameObject gameObject)
        {
            velocity = new Vector3D(gameObject.Velocity);
            acceleration = new Vector3D(gameObject.Acceleration);
            Active = false;
            componentMap = new Dictionary<string, Component>();
            Name = gameObject.Name;
            Transforms = new RelativeTransform(gameObject.Transforms);
            children = new List<GameObject>();
        }
        public RelativeTransform Transforms
        { get; set; }

        public Vector3D Velocity
        {
            get
            { return velocity; }
            set
            {
                velocity.vector.X = value.vector.X;
                velocity.vector.Y = value.vector.Y;
            }
        }
        public Vector3D Acceleration
        {
            get
            { return acceleration; }
            set
            {
                acceleration.vector.X = value.vector.X;
                acceleration.vector.Y = value.vector.Y;
            }
        }
        public void AddChild(GameObject gameObject)
        {
            gameObject.Transforms.Parent = Transforms;
            children.Add(gameObject);
            if (Active && !gameObject.Active)
            {
                MysterionEngineGame.Game.ObjectLocator.AddObject(gameObject);
            }
        }
        public void RemoveChild(GameObject gameObject)
        {
            gameObject.Transforms.Parent = null;
            children.Remove(gameObject);
            if (Active && gameObject.Active)
            {
                MysterionEngineGame.Game.ObjectLocator.RemoveObject(gameObject);
            }
        }
        public List<GameObject> Children
        { get { return children; } }
        private Dictionary<string, Component> componentMap;
        public bool Active { get; private set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public IEnumerable<Component> Components()
        {
            return componentMap.Values.ToList(); 
        }
        public Component GetComponent(string name)
        {
            return componentMap[name];
        }
        public void AddComponent(Component comp)
        {
            if (comp.Name == "")
            {
                comp.Name = componentID.ToString();
                componentID += 1;
            }
            componentMap.Add(comp.Name, comp);
            if (Active)
            {
                MysterionEngineGame.Game.ServiceLocator.AddComponent(comp);
            }
            comp.OnAdded(this);
        }
        public void RemoveComponent(Component comp)
        {
            componentMap.Remove(comp.Name);
            if (Active)
            {
                MysterionEngineGame.Game.ServiceLocator.RemoveComponent(comp);
            }
            comp.OnRemoved();
        }
        /// <summary>
        /// Called when the object is added to the engine.
        /// Be sure to call back to the base class.
        /// </summary>
        public virtual void OnActivate()
        {
            Active = true;
            foreach (GameObject gameObject in children)
            {
                MysterionEngineGame.Game.ObjectLocator.AddObject(gameObject);
            }
        }
        /// <summary>
        /// Called when the object is removed from the engine.
        /// Be sure to call back to the base class.
        /// </summary>
        public virtual void OnDeactivate()
        {
            Active = false;
            foreach (GameObject gameObject in children)
            {
                MysterionEngineGame.Game.ObjectLocator.RemoveObject(gameObject);
            }
        }
    }
}