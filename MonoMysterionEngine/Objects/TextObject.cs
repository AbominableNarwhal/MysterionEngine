﻿using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Objects
{
    public class TextObject : GameObject
    {
        public TextRenderComponent RenderComponent { get; set; }
        public TextObject()
        {
            AddRenderComponent();
        }
        public TextObject(string text, SpriteFont font)
        {
            AddRenderComponent();
            FontObj = new Font(font);
            FontObj.Text = text;
        }
        public TextObject(TextObject textObject) : base(textObject)
        {
            RenderComponent = new TextRenderComponent(textObject.RenderComponent);
            FontObj = new Font(textObject.FontObj);
            AddComponent(RenderComponent);
        }
        private void AddRenderComponent()
        {
            RenderComponent = new TextRenderComponent();
            AddComponent(RenderComponent);
        }
        public Font FontObj
        {
            get { return RenderComponent.font; }
            set { RenderComponent.font = value; }
        }
    }
}
