﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine;

namespace MonoMysterionEngine.Objects
{
    public class ObjectManager
    {
        List<GameObject> objects;
        List<GameObject> addList;
        List<GameObject> removeList;
        public ObjectManager()
        {
            objects = new List<GameObject>();
            addList = new List<GameObject>();
            removeList = new List<GameObject>();
        }
        /// <summary>
        /// Adds an Object to a list to wait to be Added
        /// </summary>
        /// <param name="obj">
        /// The object to be added
        /// </param>
        public void AddObject(GameObject obj)
        {
            addList.Add(obj);
        }
        /// <summary>
        /// Adds an Object to a list to wait to be removed
        /// </summary>
        /// <param name="obj">
        /// The object to be removed
        /// </param>
        public void RemoveObject(GameObject obj)
        {
            removeList.Add(obj);
        }
        /// <summary>
        /// Adds all Objects to a list to wait to be removed
        /// </summary>
        public void RemoveAllOjects()
        {
            foreach (GameObject obj in objects)
            {
                removeList.Add(obj);
            }
        }
        /// <summary>
        /// Adds the objects waiting to be added and
        /// removes the objects waiting to be removed
        /// </summary>
        public void UpdateContents()
        {
            while (removeList.Count > 0)
            {
                GameObject obj = removeList[0];
                obj.OnDeactivate();
                if (objects.Remove(obj))
                {
                    MysterionEngineGame.Game.ServiceLocator.RemoveComponents(obj.Components());
                }
                removeList.Remove(obj);
            }

            while (addList.Count > 0)
            {
                GameObject obj = addList[0];
                obj.OnActivate();
                objects.Add(obj);
                MysterionEngineGame.Game.ServiceLocator.AddComponents(obj.Components());
                addList.Remove(obj);
            }
        }
        public GameObject FindObjectByName(string name)
        {
            foreach (var obj in objects)
            {
                if (obj.Name.Equals(name))
                {
                    return obj;
                }
            }
            foreach (var obj in addList)
            {
                if (obj.Name.Equals(name))
                {
                    return obj;
                }
            }
            return null;
        }
    }
}
