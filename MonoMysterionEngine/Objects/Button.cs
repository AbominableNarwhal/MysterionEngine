﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Objects
{
    public class Button
    {
        private GameObject gameObject;
        private ButtonBehavior buttonBehavior;
        private SpriteRenderComponent renderComponent;
        private Box2DComponent box2DComponent;
        private TextObject textObject;
        public enum Events { Click, Cancel }
        public delegate void ButtonEventHandler(GameObject gameObject, Events buttonEvent);

        public Button()
        {
            gameObject = new GameObject();
            renderComponent = new SpriteRenderComponent();
            box2DComponent = new Box2DComponent(0, 0, 0, 0);
            buttonBehavior = new ButtonBehavior();
            textObject = new TextObject();
            ConstructGameObject();
        }
        public Button(Texture2D normalTex, Texture2D pressedTex, SpriteFont spriteFont = null, ButtonEventHandler handler = null)
        {
            gameObject = new GameObject();
            renderComponent = new SpriteRenderComponent();
            box2DComponent = new Box2DComponent(0, 0, normalTex.Width, normalTex.Height);
            buttonBehavior = new ButtonBehavior { NormalTexture = normalTex, PressedTexture = pressedTex, handler = handler };
            textObject = new TextObject("", spriteFont);
            ConstructGameObject();
        }
        public Button(Button button)
        {
            gameObject = new GameObject(button.ButtonObject);
            renderComponent = new SpriteRenderComponent(button.renderComponent);
            box2DComponent = new Box2DComponent(button.box2DComponent);
            buttonBehavior = new ButtonBehavior(button.buttonBehavior);
            textObject = new TextObject(button.textObject);
            ConstructGameObject();
        }
        private void ConstructGameObject()
        {
            gameObject.AddComponent(renderComponent);
            gameObject.AddComponent(box2DComponent);
            gameObject.AddComponent(buttonBehavior);
            gameObject.AddChild(textObject);
        }
        private void CenterText()
        {
            Vector3 imageExtent = new Vector3(NormalTex.Width, NormalTex.Height, 0);
            Vector3 textExtent = new Vector3(textObject.FontObj.Width, textObject.FontObj.Height, 0);
            textObject.Transforms.Position = (imageExtent - textExtent) / 2;
        }
        public GameObject ButtonObject
        {
            get
            {
                return gameObject;
            }
        }
        public ButtonEventHandler ClickHandler
        {
            get
            {
                return buttonBehavior.handler;
            }
            set
            {
                buttonBehavior.handler = value;
            }
        }
        public string Text
        {
            get
            {
                return textObject.FontObj.Text;
            }
            set
            {
                textObject.FontObj.Text = value;
                CenterText();
            }
        }
        public Texture2D NormalTex
        {
            get
            {
                return buttonBehavior.NormalTexture;
            }
            set
            {
                buttonBehavior.NormalTexture = value;
            }
        }
        public Texture2D PressedTex
        {
            get
            {
                return buttonBehavior.PressedTexture;
            }
            set
            {
                buttonBehavior.PressedTexture = value;
            }
        }
        public Effect ImageRenderEffect
        {
            get
            {
                return renderComponent.RenderEffect;
            }
            set
            {
                renderComponent.RenderEffect = value;
            }
        }
    }

    public class ButtonBehavior : UpdateComponent
    {

        private bool pressed = false;
        private bool hovering = false;
        public Texture2D NormalTexture { get; set; }
        public Texture2D PressedTexture { get; set; }
        public Button.ButtonEventHandler handler;
        public ButtonBehavior()
        {
            Name = "ButtonBehavior";
        }
        public ButtonBehavior(ButtonBehavior buttonBehavior)
        {
            Name = buttonBehavior.Name;
            NormalTexture = buttonBehavior.NormalTexture;
            PressedTexture = buttonBehavior.PressedTexture;
            handler = buttonBehavior.handler;
        }
        public override void Update(GameTime gameTime)
        {
            MouseState mouseState = InputSystem.Input.MouseState;
            Box2DComponent box = (Box2DComponent)Owner.GetComponent(Box2DComponent.ComponentName);
            RotatedRectangle bounds = box.Box;

            if (bounds.ContainsPoint(new Vector2(mouseState.X, mouseState.Y)))
            {
                if (mouseState.LeftButton == ButtonState.Released)
                {
                    hovering = true;
                    if (Pressed)
                    {
                        handler?.Invoke(Owner, Button.Events.Click);
                        Pressed = false;
                    }
                }
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    if (hovering)
                    {
                        Pressed = true;
                    }
                }
            }
            else
            {
                //Revert state
                if (Pressed)
                {
                    handler?.Invoke(Owner, Button.Events.Cancel);
                    Pressed = false;
                }
                hovering = false;
            }
        }
        public override void OnAdded(GameObject owner)
        {
            base.OnAdded(owner);
            Pressed = false;
        }
        public bool Pressed
        {
            get
            { return pressed; }
            private set
            {
                pressed = value;
                UpdateSprite();
            }
        }
        private void UpdateSprite()
        {
            if (Pressed)
            {
                SpriteRenderComponent renderComponent = (SpriteRenderComponent)Owner.GetComponent(SpriteRenderComponent.ComponentName);
                renderComponent.SpriteImage.Texture = PressedTexture;
            }
            else
            {
                SpriteRenderComponent renderComponent = (SpriteRenderComponent)Owner.GetComponent(SpriteRenderComponent.ComponentName);
                renderComponent.SpriteImage.Texture = NormalTexture;
            }
        }
    }
}
