﻿using MonoMysterionEngine.Utilities;
using MonoMysterionEngine.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Objects
{
    public class SpriteObject : GameObject
    {
        
        public SpriteObject()
        {
            RenderComponent = new SpriteRenderComponent();
            SpriteImage = new Sprite();
            AddComponent(RenderComponent);
        }
        public SpriteObject(SpriteObject spriteObject) : base(spriteObject)
        {
            RenderComponent = new SpriteRenderComponent(spriteObject.RenderComponent);
            AddComponent(RenderComponent);
        }
        public SpriteRenderComponent RenderComponent { get; set; }
        public Sprite SpriteImage
        {
            get
            {
                return RenderComponent.SpriteImage;
            }
            set
            {
                RenderComponent.SpriteImage = value;
            }
        }
    }
}
