﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine;

namespace MonoMysterionEngine.Systems
{
    public class WindowSystem
    {
        static WindowSystem window;
        StartUpParams param;
        GraphicsDeviceManager graphics;
        GameWindow gameWindow;
        private WindowSystem()
        { }

        public static WindowSystem Window
        {
            get
            {
                if (window == null)
                {
                    window = new WindowSystem();
                }
                return window;
            }
        }
        public StartUpParams Param
        {
            get
            {
                return param;
            }
            set
            {
                param = value;
            }
        }
        public int TargetWidth
        {
            get { return Param.WindowWidth; }
        }
        public int TargetHeight
        {
            get { return Param.WindowHeight; }
        }
        public void InitWindow(GraphicsDeviceManager _graphics, GameWindow _gameWindow)
        {
            _gameWindow.Title = param.AppName;
            _gameWindow.IsBorderless = param.Borderless;
            _gameWindow.Position = param.WindowPosition;
            graphics = _graphics;
            graphics.PreferredBackBufferWidth = param.WindowWidth;
            graphics.PreferredBackBufferHeight = param.WindowHeight;
            graphics.IsFullScreen = param.FullScreen; // set the game to full screen (6/20)
            graphics.HardwareModeSwitch = false;
            graphics.ApplyChanges();

            MysterionEngineGame.Game.IsFixedTimeStep = param.FixedFps; // set the game to modifiable fps or not (fixedtimestamp or variable time stamp) (6/22)
            if (param.FixedFps == true) // set the fps if we are using a fixed time stamp
            {
                MysterionEngineGame.Game.TargetElapsedTime = TimeSpan.FromMilliseconds(1000/param.Fps);
            }

            gameWindow = _gameWindow;
            
        }
    }
}
