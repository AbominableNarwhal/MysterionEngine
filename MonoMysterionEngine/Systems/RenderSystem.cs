﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using MonoMysterionEngine.Utilities;
//using MonoGame.Extended;

namespace MonoMysterionEngine.Systems
{
    public class RenderSystem
    {
        private static RenderSystem renderSystem;
        private SpriteBatch spriteBatch;
        private BasicEffect primitiveEffect;
        public Matrix GlobalTransform { get; set; }
        public Effect CurrentEffect { get; set; }
        private RelativeTransform localTransform;
        public ContentManager content;
        public GraphicsDevice graphicsDevice;

        //Camera2D camera;
        private RenderSystem()
        {
            localTransform = new RelativeTransform();
        }

        public static RenderSystem Renderer
        {
            get
            {
                if (renderSystem == null)
                {
                    renderSystem = new RenderSystem();
                }
                return renderSystem;
            }
        }
        /*public Camera2D Camera
        {
            get { return camera; }
        }*/
        public void ClearScreen()
        {
            spriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
        }
        public void RenderBegin()
        {
            spriteBatch.Begin(
                SpriteSortMode.Deferred,    //sortMode
                BlendState.NonPremultiplied,//blendState
                null,                       //samplerState
                null,                       //depthStencilState
                null,                       //rasterizerState
                CurrentEffect,              //effect
                GlobalTransform);           //transformMatrix
        }

        public void RenderEnd()
        {
            spriteBatch.End();
        }
        private void PrepareDraw(VertexBuffer vertexBuffer, IndexBuffer indexBuffer,
            Vector2 pos, Vector2 scale, float rotation)
        {
            localTransform.Position = new Vector3(pos, 0);
            localTransform.Scale = new Vector3(scale, 0);
            localTransform.Rotation = rotation;
            graphicsDevice.SetVertexBuffer(vertexBuffer);
            graphicsDevice.Indices = indexBuffer;
            primitiveEffect.View = GlobalTransform;
            primitiveEffect.World = localTransform.LocalTransform;
        }
        /*Do not call this function after RenderBegin*/
        public void RenderPrimitive(VertexBuffer vertexBuffer, Vector2 pos, Vector2 scale, float rotation, int offset, int count)
        {
            PrepareDraw(vertexBuffer, null, pos, scale, rotation);

            foreach (EffectPass pass in primitiveEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, offset, count);
            }
        }
        /*Do not call this function after RenderBegin*/
        public void RenderIndexedPrimitive(VertexBuffer vertexBuffer, IndexBuffer indexBuffer, Vector2 pos,
            Vector2 scale, float rotation, int offset, int startIndex, int count)
        {
            PrepareDraw(vertexBuffer, indexBuffer, pos, scale, rotation);

            foreach (EffectPass pass in primitiveEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, offset, startIndex, count);
            }
        }
        public void RenderSprite(Sprite sprite, Vector2 pos, Vector2 scale, float rotation, int renderIndex = 0)
        {
            if (sprite.Texture != null)
            {
                spriteBatch.Draw(sprite.Texture,    //texture
                    pos,                            //position
                    sprite.SourceRectangle,         //Source Rectangle
                    sprite.Color,                   //Color
                    rotation,                       //Rotation
                    sprite.Center.vector,           //scale/rotation center
                    scale,                          //Scaling
                    sprite.Effect(),                //Horizontal or vertical flip
                    renderIndex);                   //Draw Layer
            }
        }

        public void RenderText(Font font, Vector2 pos, Vector2 scale, float rotation)
        {
            if (font != null)
            {
                spriteBatch.DrawString(
                    font.spriteFont,
                    font.Text,
                    pos,
                    font.Color,
                    rotation,
                    font.Center.vector,
                    scale,
                    font.Effect(),
                    0);
            }
        }

        public void InitRenderer(SpriteBatch spriteBatch, ContentManager content, GraphicsDevice graphicsDevice)
        {
            this.spriteBatch = spriteBatch;
            //camera = new Camera2D(spriteBatch.GraphicsDevice);
            this.content = content;
            this.graphicsDevice = graphicsDevice;

            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.None;
            graphicsDevice.RasterizerState = rasterizerState;
            GlobalTransform = Matrix.Identity;

            primitiveEffect = new BasicEffect(graphicsDevice);
            float halfWidth = WindowSystem.Window.TargetWidth / 2.0f;
            float halfHeight = WindowSystem.Window.TargetHeight / 2.0f;

            primitiveEffect.World = Matrix.Identity;
            primitiveEffect.View = Matrix.Identity;
            primitiveEffect.Projection = Matrix.CreateScale(1 / halfWidth, -1 / halfHeight, 0) * Matrix.CreateTranslation(-1, 1, 0);
            primitiveEffect.VertexColorEnabled = true;
        }
        public Texture2D LoadTexture(string name)
        {
            try
            {
                return content.Load<Texture2D>(name);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public void UnloadResources()
        {
            content.Unload();
        }
    }
}
