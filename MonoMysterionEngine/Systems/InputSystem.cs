﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace MonoMysterionEngine.Systems
{
    public class InputSystem
    {
        static InputSystem inputSystem;
        public KeyboardState KeyboardState { get; set; }
        public MouseState MouseState { get; set; }
        private InputSystem()
        { }

        public static InputSystem Input
        {
            get
            {
                if (inputSystem == null)
                {
                    inputSystem = new InputSystem();
                }
                return inputSystem;
            }
        }

        public void GrabInput()
        {
            KeyboardState = Keyboard.GetState();
            MouseState = Mouse.GetState();
        }

    }
}
