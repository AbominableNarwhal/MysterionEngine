﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace MonoMysterionEngine.Components
{
    public class BasicInputComponent : InputComponent
    {
        public delegate void InputHandler(KeyboardState keyboardState, MouseState mouseState);
        private InputHandler handler;
        public BasicInputComponent(InputHandler handler)
        {
            this.handler = handler;
        }
        public override void InputStateUpdate(KeyboardState keyboardState, MouseState mouseState)
        {
            handler(keyboardState, mouseState);
        }
        public void RemoveHandler()
        {
            handler = null;
        }
    }
}
