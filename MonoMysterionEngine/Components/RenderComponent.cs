﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public abstract class RenderComponent : Component
    {
        public RenderComponent()
        {
            Tag = "Render";
            Name = "Render";
            Visable = true;
        }
        public RenderComponent(RenderComponent renderComponent)
        {
            Tag = renderComponent.Tag;
            Visable = renderComponent.Visable;
        }
        public bool Visable { get; set; }
        public int Index { get; set; }
        public Effect RenderEffect { get; set; }
        public abstract void Draw(RenderSystem renderSystem);
    }
}
