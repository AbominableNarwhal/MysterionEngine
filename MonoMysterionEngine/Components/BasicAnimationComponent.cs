﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace MonoMysterionEngine.Components
{
    public class BasicAnimationComponent : AnimationComponent
    {
        public delegate bool AnimateHandler(GameTime gameTime);
        public delegate void CompleteHandler(BasicAnimationComponent component);
        private AnimateHandler animateHandler;
        private CompleteHandler completeHandler;

        public BasicAnimationComponent(AnimateHandler animateHandler, CompleteHandler completeHandler, long duration = 0) : base(duration)
        {
            this.animateHandler = animateHandler;
            this.completeHandler = completeHandler;
        }
        public override void Complete()
        {
            completeHandler?.Invoke(this);
        }
        public override bool Animate(GameTime gameTime)
        {
            bool? result = animateHandler?.Invoke(gameTime);
            if (result != null)
            {
                return (bool)result;
            }
            return false;
        }
    }
}
