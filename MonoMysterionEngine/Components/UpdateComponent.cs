﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Components
{
    public abstract class UpdateComponent : Component
    {
        public UpdateComponent()
        {
            Tag = "Update";
        }
        public abstract void Update(GameTime gameTime);
    }
}
