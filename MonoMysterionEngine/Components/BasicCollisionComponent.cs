﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public class BasicCollisionComponent : CollisionComponent
    {
        CollisionHandler handler;
        public BasicCollisionComponent(GameObject subject, CollisionHandler handler) : base(subject)
        {
            this.handler = handler;
        }
        public override void OnCollisionEnter(GameObject other, GameBox mine, GameBox otherBox)
        {
            handler.OnCollisionEnter(other, mine, otherBox);
        }
        public override void OnCollisionExit(GameObject other, GameBox mine, GameBox otherBox)
        {
            handler.OnCollisionExit(other, mine, otherBox);
        }
    }

    public interface CollisionHandler
    {
        void OnCollisionEnter(GameObject other, GameBox mine, GameBox otherBox);
        void OnCollisionExit(GameObject other, GameBox mine, GameBox otherBox);
    }
}
