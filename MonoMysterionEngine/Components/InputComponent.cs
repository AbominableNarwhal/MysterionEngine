﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace MonoMysterionEngine.Components
{
    public abstract class InputComponent : Component
    {
        public InputComponent()
        {
            Tag = "Input";
            Name = "Input";
        }
        public abstract void InputStateUpdate(KeyboardState keyboardState, MouseState mouseState);
    }
}
