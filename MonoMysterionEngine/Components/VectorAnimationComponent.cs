﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public class VectorAnimationComponent : AnimationComponent
    {
        public bool Loop { get; set; }
        public Vector2 FromVec;
        public Vector2 ToVec;
        public Vector2D ResultVec { get; private set; }
        public double CurTime { get; private set; }
        public double TotalTime { get; private set; }
        public VectorAnimationComponent() { }
        public VectorAnimationComponent(Vector2 fromVec, Vector2 toVec, Vector2D resultVec, double totalTime, GameObject owner = null, bool loop = false)
        {
            FromVec = fromVec;
            ToVec = toVec;
            TotalTime = totalTime;
            Owner = owner;
            ResultVec = resultVec;
            Loop = loop;
        }
        public override bool Animate(GameTime gameTime)
        {
            CurTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            float delta = (float)(CurTime / TotalTime);
            if (delta >= 1)
            {
                delta = 1;
            }
            Vector2.Lerp(ref FromVec, ref ToVec, delta, out ResultVec.vector);
            if (delta == 1)
            {
                if(!Loop)
                    return false;
                else
                    CurTime = 0;
            }
            return true;
        }
    }
}
