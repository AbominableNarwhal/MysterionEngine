﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Utilities;
using MonoMysterionEngine.Objects;

namespace MonoMysterionEngine.Components
{
    public class CollisionComponent : Component
    {
        List<GameBox> boxes;
        GameObject subject;
        public CollisionComponent(GameObject subject)
        {
            this.subject = subject;
            Tag = "Collision";
            boxes = new List<GameBox>();
        }
        public List<GameBox> BoundingBoxes
        {
            get { return boxes; }
        }
        public GameObject Subject
        {
            get { return subject; }
        }
        public void DetectCollision(CollisionComponent other)
        {
            foreach (var myBox in boxes)
            {
                foreach (var otherBox in other.BoundingBoxes)
                {
                    if (myBox.Collision(otherBox))
                    {
                        if (!myBox.HasPartner(otherBox))
                        {
                            myBox.addPartner(otherBox);
                            otherBox.addPartner(myBox);
                            OnCollisionEnter(other.Subject, myBox, otherBox);
                            other.OnCollisionEnter(subject, otherBox, myBox);
                        }
                    }
                    else
                    {
                        if (myBox.HasPartner(otherBox))
                        {
                            myBox.RemovePartner(otherBox);
                            otherBox.RemovePartner(myBox);
                            OnCollisionExit(other.Subject, myBox, otherBox);
                            other.OnCollisionExit(subject, otherBox, myBox);
                        }
                    }
                }
            }
        }
        public virtual void OnCollisionEnter(GameObject other, GameBox mine, GameBox otherBox) { }
        public virtual void OnCollisionExit(GameObject other, GameBox mine, GameBox otherBox) { }
    }
}
