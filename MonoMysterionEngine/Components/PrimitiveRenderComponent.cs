﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Components
{
    public class PrimitiveRenderComponent : RenderComponent
    {
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        private PrimitiveGeometry geometry;
        public PrimitiveRenderComponent() { }
        public PrimitiveGeometry Geometry
        {
            get
            {
                return geometry;
            }
            set
            {
                geometry = value;
                if (geometry.vertices != null)
                {
                    vertexBuffer = new VertexBuffer(RenderSystem.Renderer.graphicsDevice, typeof(VertexPositionColor),
                        geometry.vertices.Length, BufferUsage.WriteOnly);
                    vertexBuffer.SetData(geometry.vertices);
                }
                if (geometry.indices != null)
                {
                    indexBuffer = new IndexBuffer(RenderSystem.Renderer.graphicsDevice, typeof(short), geometry.indices.Length, BufferUsage.WriteOnly);
                    indexBuffer.SetData(geometry.indices);
                }
            }
        }
        public override void Draw(RenderSystem renderSystem)
        {
            if (vertexBuffer != null)
            {
                if (indexBuffer != null)
                {
                    renderSystem.RenderIndexedPrimitive(vertexBuffer, indexBuffer, new Vector2(0, 0), new Vector2(1, 1),
                        0, 0, 0, geometry.TriangleCount);
                }
                else
                {
                    renderSystem.RenderPrimitive(vertexBuffer, new Vector2(0, 0), new Vector2(1, 1), 0, 0, geometry.TriangleCount);
                }
            }
        }
    }
}
