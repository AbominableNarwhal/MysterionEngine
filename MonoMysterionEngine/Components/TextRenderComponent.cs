﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Components 
{
    public class TextRenderComponent : RenderComponent
    {
        public Font font;
        public TextRenderComponent()
        { }
        public TextRenderComponent(TextRenderComponent renderComponent) : base(renderComponent)
        {
            font = renderComponent.font;
        }
        public override void Draw(RenderSystem renderSystem)
        {
            renderSystem.RenderBegin();
            if (font != null && font.Text != "")
            {
                renderSystem.RenderText(font, new Vector2(0, 0), new Vector2(1, 1), 0);
            }
            renderSystem.RenderEnd();
        }
    }
}
