﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public class SpriteRenderComponent : RenderComponent
    {
        public static readonly string ComponentName = "sprite render";
        public Sprite SpriteImage { get; set; }
        public SpriteRenderComponent()
        {
            SpriteImage = new Sprite();
            Name = "sprite render";
        }
        public SpriteRenderComponent(SpriteRenderComponent renderComponent) : base(renderComponent)
        {
            SpriteImage = new Sprite(renderComponent.SpriteImage);
            Name = renderComponent.Name;
        }
        public override void Draw(RenderSystem renderSystem)
        {
            if (RenderEffect != null)
            {
                RenderEffect.Parameters["ScreenTexture"].SetValue(SpriteImage.Texture);
            }
            renderSystem.RenderBegin();
            if (SpriteImage != null && Transforms != null)
            {
                renderSystem.RenderSprite(SpriteImage, new Vector2(0, 0), new Vector2(1, 1), 0, Index);
            }
            renderSystem.RenderEnd();
        }
    }
}
