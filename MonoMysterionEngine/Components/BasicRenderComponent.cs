﻿using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public class BasicRenderComponent : RenderComponent
    {
        public delegate void DrawHandler(RenderSystem renderSystem);
        private DrawHandler handler;
        public BasicRenderComponent(DrawHandler handler) : base()
        {
            this.handler = handler;
        }
        public override void Draw(RenderSystem renderSystem)
        {
            handler(renderSystem);
        }
        public void RemoveHandler()
        {
            handler = null;
        }
    }
}
