﻿using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Components
{
    public class Component
    {
        public GameObject Owner { get; protected set; }
        public string Tag { get; protected set; }
        public string Name { get; set; }
        public Component()
        {
            Tag = "default";
            Name = "";
        }
        public RelativeTransform Transforms
        {
            get
            {
                return Owner.Transforms;
            }
        }
        public virtual void OnAdded(GameObject owner)
        {
            Owner = owner;
        }
        public virtual void OnRemoved()
        {
            Owner = null;
        }
    }
}
