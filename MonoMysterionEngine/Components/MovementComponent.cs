﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoMysterionEngine.Utilities;
using Microsoft.Xna.Framework;

namespace MonoMysterionEngine.Components
{
    public class MovementComponent : AnimationComponent
    {
        public Vector2D Position { get; private set; }
        public Vector2D Velocity { get; private set; }
        public Vector2D Acceloration { get; private set; }
        public MovementComponent()
        { }
        public MovementComponent(Vector2D position, Vector2D velocity, Vector2D acceloration)
        {
            this.Position = position;
            this.Velocity = velocity;
            this.Acceloration = acceloration;
        }
        public void SetVectorPointers(Vector2D position, Vector2D velocity, Vector2D acceloration)
        {
            this.Position = position;
            this.Velocity = velocity;
            this.Acceloration = acceloration;
        }
        public override bool Animate(GameTime gameTime)
        {
            //get the fraction of a second
            float secondFraction = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000;
            //calc velocity
            Velocity.vector.X += Acceloration.vector.X * secondFraction;
            Velocity.vector.Y += Acceloration.vector.Y * secondFraction;
            //calc position
            Position.vector.X += Velocity.vector.X * secondFraction;
            Position.vector.Y += Velocity.vector.Y * secondFraction;
            return true;
        }
    }
}
