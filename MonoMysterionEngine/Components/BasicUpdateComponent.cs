﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Components
{
    public class BasicUpdateComponent : UpdateComponent
    {
        public BasicUpdateHandler Handler { get; set; }
        public BasicUpdateComponent(BasicUpdateHandler handler)
        {
            Handler = handler;
        }
        public override void Update(GameTime gameTime)
        {
            Handler.OnUpdate(gameTime);
        }
        public override void OnRemoved()
        {
            base.OnRemoved();
            Handler = null;
        }
        public interface BasicUpdateHandler
        {
            void OnUpdate(GameTime gameTime);
        }
    }
}
