﻿using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoMysterionEngine.Components
{
    public class Box2DComponent : Component
    {
        public static readonly string ComponentName = "Box2D";
        public RotatedRectangle Box { get; set; }
        public Box2DComponent(float ox, float oy, float width, float height)
        {
            Name = ComponentName;
            Box = new RotatedRectangle(null, ox, oy, width, height);
        }
        public Box2DComponent(Box2DComponent box2DComponent) : this(box2DComponent.Box.tl.X, box2DComponent.Box.tl.Y,
            box2DComponent.Box.Width, box2DComponent.Box.Height)
        {
            Name = box2DComponent.Name;
        }
        public Box2DComponent() : this(0, 0, 0, 0)
        {}

        public override void OnAdded(GameObject owner)
        {
            base.OnAdded(owner);
            Box.transform = Transforms;
        }
    }
}
