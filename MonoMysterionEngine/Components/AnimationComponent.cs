﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Objects;

namespace MonoMysterionEngine.Components
{
    public abstract class AnimationComponent : Component
    {
        public AnimationComponent(long duration = 0)
        {
            Tag = "Animate";
            Name = "Animate";
            Duration = duration;
        }
        public double Duration { get; protected set; }
        public double CurrentTime { get; protected set; }
        public double Progress
        {
            get
            {
                if (Duration != 0)
                {
                    return CurrentTime / Duration;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                CurrentTime = Duration * value;
            }
        }
        public virtual void Complete() { }
        public abstract bool Animate(GameTime gameTime);
    }
}
