﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Systems;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public class SDFFontRenderComponent : RenderComponent
    {
        public SDFFont FontDesc
        {
            get { return SDFTextObj.Font; }
        }
        public string Text
        {
            get { return SDFTextObj.Text; }
            set { SDFTextObj.Text = value; }
        }
        public SDFText SDFTextObj
        {
            get; set;
        }
        private Sprite glyphSprite;

        public SDFFontRenderComponent()
        {
            Init();
        }

        public SDFFontRenderComponent(SDFFont font, string text = "")
        {
            Init(font, text);
        }

        private void Init(SDFFont font = null, string text = "")
        {
            SDFTextObj = new SDFText(font);
            Text = text;
            glyphSprite = new Sprite();
            RenderEffect = new SDFFontEffect(RenderSystem.Renderer.graphicsDevice);
            if (FontDesc != null && FontDesc.FontTexture != null)
            {
                glyphSprite.Texture = FontDesc.FontTexture;
            }
        }

        public override void Draw(RenderSystem renderSystem)
        {
            RenderEffect.Parameters["FontTexture"].SetValue(glyphSprite.Texture);
            renderSystem.RenderBegin();

            IEnumerable<Vector2> rederData = SDFTextObj.GetTextLayout();
            int i = 0;
            foreach (Vector2 characterPos in rederData)
            {
                char character = Text[i];
                SDFGlyph glyph = FontDesc.Glyphs[character];
                
                Rectangle rect = new Rectangle(glyph.x, glyph.y, glyph.width, glyph.height);
                glyphSprite.SourceRectangle = rect;
                renderSystem.RenderSprite(glyphSprite, characterPos, new Vector2(SDFTextObj.Scale, SDFTextObj.Scale), 0, Index);
                i += 1;
            }
            renderSystem.RenderEnd();
        }
    }
}
