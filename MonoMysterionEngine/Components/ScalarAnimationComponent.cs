﻿using Microsoft.Xna.Framework;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;

namespace MonoMysterionEngine.Components
{
    public class ScalarAnimationComponent : AnimationComponent
    {
        public bool Loop { get; set; }
        public float FromVal { get; set; }
        float diff;
        public Scalar ResultVal { get; set; }
        public double CurTime { get; set; }
        public double TotalTime { get; set; }
        public ScalarAnimationComponent()
        {}
        public ScalarAnimationComponent(float _fromVal, float _toVal, Scalar refVal, GameObject owner = null, bool loop = false)
        {
            ResultVal = refVal;
            FromVal = _fromVal;
            diff = _toVal - _fromVal;
            Owner = owner;
            Loop = loop;
        }
        public override bool Animate(GameTime gameTime)
        {
            CurTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            float delta = (float)(CurTime / TotalTime);
            if (delta >= 1)
            {
                delta = 1;
            }
            ResultVal.Value = FromVal + diff * delta;
            if (delta == 1)
            {
                if (!Loop)
                    return false;
                else
                    CurTime = 0;
            }
            return true;
        }
    }
}
